package com.sergey.javacourse.task4.resources;

public enum AircraftType {
    CESSNA,
    BOEING,
}
