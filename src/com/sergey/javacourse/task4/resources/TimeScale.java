package com.sergey.javacourse.task4.resources;

public class TimeScale {
    /**
     * 1- normal time
     * >1 - slower
     * <1 - faster
     */
    private static final double TIME_SCALE = 0.01;
    private static final int STANDARD_TIME = 1000;

    public static int getTime(int seconds) {
        return (int) (seconds*STANDARD_TIME*TIME_SCALE);
    }
}
