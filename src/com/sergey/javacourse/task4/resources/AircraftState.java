package com.sergey.javacourse.task4.resources;

public enum AircraftState {
    READY,
    CRUSHED,
    LANDED,
    SECOND_CIRCLE,
    SPARE_AIRPORT,
}
