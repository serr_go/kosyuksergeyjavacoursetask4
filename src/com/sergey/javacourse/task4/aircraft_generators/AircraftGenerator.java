package com.sergey.javacourse.task4.aircraft_generators;

import com.sergey.javacourse.task4.aircrafts.Aircraft;
import com.sergey.javacourse.task4.aircrafts.Boeing;
import com.sergey.javacourse.task4.airports.Airport;
import com.sergey.javacourse.task4.resources.AircraftType;
import com.sergey.javacourse.task4.aircrafts.Cessna;

import java.util.Timer;
import java.util.TimerTask;
import java.util.logging.Level;
import java.util.logging.Logger;

public class AircraftGenerator {
    private static final Logger LOGGER =
            Logger.getLogger(AircraftGenerator.class.getName());

    public AircraftGenerator(int amountOfAircraft, int aircraftGenerationDelay, AircraftType type, Airport airport) {
        final int[] currentAircraft = {0};

        Timer generator = new Timer();
        generator.scheduleAtFixedRate(new TimerTask() {
            @Override
            public void run() {
                currentAircraft[0]++;
                if (amountOfAircraft <= currentAircraft[0]) {
                    generator.cancel();
                    generator.purge();
                }

                airport.takeAircraft(generateAircraft(type));
                generateAircraft(type);
            }
        }, aircraftGenerationDelay, aircraftGenerationDelay);
    }

    private Aircraft generateAircraft(AircraftType type) {
        switch (type) {
            case CESSNA:
                return new Cessna();
            case BOEING:
                return new Boeing();
            default:
                LOGGER.log(Level.WARNING, type.toString() + " unknown type of aircraft.");
                return null;
        }
    }
}
