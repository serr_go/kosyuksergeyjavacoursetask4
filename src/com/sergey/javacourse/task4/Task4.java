package com.sergey.javacourse.task4;

import com.sergey.javacourse.task4.aircraft_generators.AircraftGenerator;
import com.sergey.javacourse.task4.airports.Airport;

import java.io.IOException;
import java.util.logging.LogManager;
import java.util.logging.Logger;

import static com.sergey.javacourse.task4.resources.AircraftType.*;
import static com.sergey.javacourse.task4.resources.TimeScale.getTime;

public class Task4 {
    private static final int AMOUNT_OF_AIRCRAFT = 5;
    private static final int AIRCRAFT_GENERATION_DELAY = getTime(2*60);
    private static final Logger LOGGER =
            Logger.getLogger(Task4.class.getName());

    public static void main(String[] args) {
        try {
            LogManager.getLogManager().readConfiguration(
                    Task4.class.getResourceAsStream("/com/sergey/javacourse/task4/resources/logging.properties"));
        } catch (IOException e) {
            System.err.println("Could not setup logger configuration: " + e.toString());
        }

        Airport airport = new Airport();

        AircraftGenerator cessnaGenerator = new AircraftGenerator(
                AMOUNT_OF_AIRCRAFT,
                AIRCRAFT_GENERATION_DELAY,
                CESSNA,
                airport
        );
        /*AircraftGenerator boeingGenerator = new AircraftGenerator(
                AMOUNT_OF_AIRCRAFT,
                AIRCRAFT_GENERATION_DELAY,
                BOEING,
                airport
        );*/
    }
}
