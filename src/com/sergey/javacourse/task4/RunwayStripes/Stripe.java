package com.sergey.javacourse.task4.RunwayStripes;

import java.util.Timer;
import java.util.TimerTask;

public class Stripe {
    private int length;
    private boolean busy;

    public Stripe(int length) {
        this.length = length;
        this.busy = false;
    }

    public int getLength() {
        return length;
    }

    public synchronized void setBusy(int time) {
        busy = true;

        Timer landing = new Timer();
        landing.schedule(new TimerTask() {
            @Override
            public void run() {
                busy = false;
                landing.cancel();
                landing.purge();
            }
        }, time);
    }

    public synchronized boolean isBusy() {
        return busy;
    }
}
