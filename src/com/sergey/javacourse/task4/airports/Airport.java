package com.sergey.javacourse.task4.airports;

import com.sergey.javacourse.task4.RunwayStripes.Stripe;
import com.sergey.javacourse.task4.aircrafts.Aircraft;
import com.sergey.javacourse.task4.dispatchers.Dispatcher;

import java.util.*;
import java.util.logging.Level;
import java.util.logging.Logger;

import static com.sergey.javacourse.task4.resources.AircraftState.*;
import static com.sergey.javacourse.task4.resources.TimeScale.*;
import static java.util.Comparator.comparing;

public class Airport {
    private static final int SECOND_CIRCLE_TIME = getTime(7*60);
    private static final int SPARE_AIRPORT_DISTANCE = getTime(30*60);

    //Magic parameters
    private static final int LONG_BUSY_PARAMETER = 100;
    private static final int SHORT_BUSY_PARAMETER = 100;
    private static final int SPARE_AIRPORT_PARAMETER = 5;

    private static final Logger LOGGER =
            Logger.getLogger(Airport.class.getName());

    private Dispatcher dispatcher;
    private ArrayList<Aircraft> aircraftUnderControl;
    private ArrayList<Aircraft> aircraftHistory;
    private Stripe shortStripe;
    private Stripe longStripe;

    public Airport() {
        dispatcher = new Dispatcher();
        aircraftUnderControl = new ArrayList<>();
        aircraftHistory = new ArrayList<>();
        shortStripe = new Stripe(800);
        longStripe = new Stripe(1200);
    }

    public synchronized void takeAircraft(Aircraft aircraft) {
        aircraftUnderControl.add(aircraft);
        aircraftUnderControl.sort(comparing(Aircraft::getFuel));
        aircraftHistory.add(aircraft);

        dispatcher.setCourse();
    }

    public synchronized void checkCrushes() {
        Iterator<Aircraft> it = aircraftUnderControl.iterator();
        while (it.hasNext()) {
            Aircraft aircraft = it.next();
            if (aircraft.getState() == CRUSHED)
                it.remove();
        }
    }

    public synchronized void checkSpares() {
        Iterator<Aircraft> it = aircraftUnderControl.iterator();
        while (it.hasNext()) {
            Aircraft aircraft = it.next();
            if(aircraft.getState() == SPARE_AIRPORT && (aircraft.getFuel() % SECOND_CIRCLE_TIME == 0)) {
                LOGGER.log(Level.FINE, aircraft.getAircraftID() + " aircraft with " + aircraft.getFuel() + " fuel was sent to spare airport.");
                it.remove();
            }
        }
    }

    public synchronized void setSpares(int count, boolean isNeedSpareAirport) {

        for (Aircraft v : aircraftUnderControl) {
            if (v.getFuel() >= SPARE_AIRPORT_DISTANCE
                    || v.getFuel() < SPARE_AIRPORT_DISTANCE + SECOND_CIRCLE_TIME) {
                count++;
            }

            if ((v.getState() == SECOND_CIRCLE) && count >= SPARE_AIRPORT_PARAMETER) {
                if (v.getFuel() >= SPARE_AIRPORT_DISTANCE + SECOND_CIRCLE_TIME)
                    break;
                else if (v.getFuel() >= SPARE_AIRPORT_DISTANCE) {

                    if (isNeedSpareAirport)
                        v.setState(SPARE_AIRPORT);
                    isNeedSpareAirport = true;
                }
            }
        }
    }

    public synchronized Aircraft setSecondCircles() {
        Aircraft prev = null;
        Aircraft aircraft = null;

        for(Aircraft v: aircraftUnderControl) {
            if (v.getState() == SECOND_CIRCLE && prev == null) {
                prev = v;
            }
            if(v.getState() == READY ) {
                aircraft = v;
                if (prev != null) {
                    if (aircraftUnderControl.get(0).getFuel() < SECOND_CIRCLE_TIME*2)
                        shortStripe.setBusy(LONG_BUSY_PARAMETER);
                    else if (prev.getFuel() < SECOND_CIRCLE_TIME*2)
                        shortStripe.setBusy(LONG_BUSY_PARAMETER);
                    else if ((prev.getFuel() % SECOND_CIRCLE_TIME == 1000) && (prev.getFuel() < aircraft.getFuel()))
                        shortStripe.setBusy(SHORT_BUSY_PARAMETER);
                }
                break;
            }
        }

        return aircraft;
    }
}
