package com.sergey.javacourse.task4.dispatchers;

import com.sergey.javacourse.task4.aircrafts.Aircraft;

import java.util.Timer;
import java.util.TimerTask;
import java.util.logging.Level;

import static com.sergey.javacourse.task4.resources.AircraftState.*;
import static com.sergey.javacourse.task4.resources.AircraftState.LANDED;

public class Dispatcher {

    // TODO make Dispatcher works

    public synchronized void setCourse() {
        Aircraft aircraft = null;

        checkCrushes();
        checkSpares();

        int count = 0;
        boolean isNeedSpareAirport = false;

        setSpares(count, isNeedSpareAirport);

        aircraft = setSecondCircles();

        if (aircraft != null){
            if (shortStripe.isBusy()) {
                if (aircraft.getState() != SPARE_AIRPORT && aircraft.getState() != CRUSHED && aircraft.getState() != LANDED) {
                    aircraft.setState(SECOND_CIRCLE);
                    LOGGER.log(Level.WARNING, aircraft.getAircraftID() + " aircraft on second circle.");

                    Timer secondCircle = new Timer();
                    Aircraft finalAircraft = aircraft;
                    secondCircle.schedule(new TimerTask() {
                        @Override
                        public void run() {
                            if (finalAircraft.getState() == SECOND_CIRCLE)
                                finalAircraft.setState(READY);
                            else if (finalAircraft.getState() == SPARE_AIRPORT)
                                finalAircraft.stopFlight();
                            setCourse();

                            secondCircle.cancel();
                            secondCircle.purge();
                        }
                    }, SECOND_CIRCLE_TIME);
                }
            } else {
                //if (aircraft.getFuel() % SECOND_CIRCLE_TIME == 0)
                shortStripe.setBusy(aircraft.getLandingTime());
                aircraft.stopFlight();
                aircraft.setState(LANDED);
                LOGGER.log(Level.FINEST, aircraft.getAircraftID() + " aircraft landed. Fuel " + aircraft.getFuel());
                aircraftUnderControl.remove(aircraft);
            }
        }
    }
}
