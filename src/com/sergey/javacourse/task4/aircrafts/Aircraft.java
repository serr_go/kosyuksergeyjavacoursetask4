package com.sergey.javacourse.task4.aircrafts;

import com.sergey.javacourse.task4.resources.AircraftState;
import com.sergey.javacourse.task4.resources.AircraftType;

import java.util.Timer;
import java.util.TimerTask;
import java.util.logging.Level;
import java.util.logging.Logger;

import static com.sergey.javacourse.task4.resources.AircraftState.*;
import static java.util.SimpleTimeZone.*;

public abstract class Aircraft implements Comparable<Aircraft> {
    private static final Logger LOGGER =
            Logger.getLogger(Aircraft.class.getName());

    private static final Object counterLock = new Object();
    private static volatile int aircraftNum = 0;
    private int fuel;
    private int stripLength;
    private int landingTime;
    private AircraftState state;
    private Timer flight;
    private String aircraftID;

    public Aircraft(AircraftType aircraftType, int fuel, int runwayStripLength, int landingTime) {
        this.fuel = fuel;
        this.stripLength = runwayStripLength;
        this.landingTime = landingTime;
        this.state = READY;
        setAircraftID(aircraftType);
        LOGGER.log(Level.FINE, aircraftID+" created at the time "+ System.currentTimeMillis());
        startFlight();
    }

    private void setAircraftID(AircraftType aircraftType){
        synchronized (counterLock) {
            aircraftNum++;
        }
        this.aircraftID = aircraftType+"_"+aircraftNum;
    }

    @Override
    public String toString() {
        return aircraftID+" "+fuel+" "+state;
    }

    @Override
    public int compareTo(Aircraft aircraft) {
        if (this.fuel == aircraft.fuel) return 0;
        else
            if (this.fuel < aircraft.fuel) return -1;
        else
            return 1;
    }

    private void startFlight() {
        flight = new Timer();
        flight.scheduleAtFixedRate(new TimerTask() {
            @Override
            public void run() {
                if (fuel != 0) {
                    fuel -= STANDARD_TIME;
                }
                else {
                    state = CRUSHED;
                    LOGGER.log(Level.SEVERE, aircraftID + " aircraft crushed at the time " + System.currentTimeMillis());
                    flight.cancel();
                    flight.purge();
                }
            }
        }, STANDARD_TIME, STANDARD_TIME);
    }

    public void stopFlight() {
        flight.cancel();
        flight.purge();
    }

    public int getFuel() {
        return fuel;
    }

    public int getStripLength() {
        return stripLength;
    }

    public int getLandingTime() {
        return landingTime;
    }

    public synchronized void setState(AircraftState state) {
        LOGGER.log(Level.FINE, aircraftID + " state changed to " + state + " at the time " + System.currentTimeMillis());
        this.state = state;
    }

    public synchronized AircraftState getState() {
        return state;
    }

    public String getAircraftID() {
        return aircraftID;
    }
}
