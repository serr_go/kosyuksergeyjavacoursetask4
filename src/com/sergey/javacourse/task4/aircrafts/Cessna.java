package com.sergey.javacourse.task4.aircrafts;

import static com.sergey.javacourse.task4.resources.TimeScale.*;
import static com.sergey.javacourse.task4.resources.AircraftType.CESSNA;

public class Cessna extends Aircraft {
    private static final int RUNWAY_STRIP_LENGTH = 800;
    private static final int LENDING_TIME = getTime(60);
    private static final int FUEL = getTime(50*60);

    public Cessna() {
        super(CESSNA, FUEL, RUNWAY_STRIP_LENGTH, LENDING_TIME);
    }
}
