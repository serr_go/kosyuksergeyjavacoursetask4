package com.sergey.javacourse.task4.aircrafts;

import static com.sergey.javacourse.task4.resources.TimeScale.*;
import static com.sergey.javacourse.task4.resources.AircraftType.BOEING;

public class Boeing extends Aircraft {
    private static final int RUNWAY_STRIP_LENGTH = 1000;
    private static final int LENDING_TIME = getTime(65);
    private static final int FUEL = getTime(50*60);

    public Boeing() {
        super(BOEING, FUEL, RUNWAY_STRIP_LENGTH, LENDING_TIME);
    }
}
